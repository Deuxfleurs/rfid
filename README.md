C'est un site statique généré par [hugo](https://gohugo.io/) que vous devez installer. Par exemple sous Fedora :

```bash
sudo dnf install hugo
```

Vous pouvez générer le site simplement :

```bash
hugo
```

Pour avoir un serveur web lors du développement :

```bash
hugo server
```

Pour déployer :

```bash
export AWS_ACCESS_KEY_ID=xxxx
export AWS_SECRET_ACCESS_KEY=xxx

hugo
hugo deploy
```
