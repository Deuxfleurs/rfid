---
title: 11 décembre 2021 - Identité numérique souveraine
summary: "En réponse à la centralisation induite par l'authentification unique, des protocoles appelés \"identité numériques souveraines\" basés sur la cryptographie et non plus sur un tiers de confiance ont été proposés. Lors de cette rencontre, nous souhaitons interroger le potentiel de décentralisation de cette technologie ainsi que la place qu'elle peut prendre dans un l'écosystème du libre."
---

**Date** - Samedi 11 Décembre 2021  
**Heure** - 10h à Québec, 16h à Paris  
**Durée** - Environ 2h  
**Lieu** - En visio sur le Jitsi de Fédi Québec : [https://jitsi.fedi.quebec/infonuagique](https://jitsi.fedi.quebec/infonuagique)  

---

## Programme

Mot de passe perdu, hameçonnage, fuite de données : l'identification sur Internet reste aujourd'hui un point de friction pour bon nombre d'utilisatrices et utilisateurs. En réponse, des systèmes d'authentification unique ont été conçus et principalement investis par des grandes entreprises du numérique. Ces derniers, en échange de la simplification des usages, ont eu pour effet de re-centraliser un peu plus Internet.

En réponse à la centralisation induite par l'authentification unique, des protocoles appelés "identité numériques souveraines" basés sur la cryptographie et non plus sur un tiers de confiance ont été proposés. Lors de cette rencontre, nous souhaitons interroger le potentiel de décentralisation de cette technologie ainsi que la place qu'elle peut prendre dans l'écosystème du libre.

<!-- Plusieurs modèles de décentralisation ont été mises de l'avant, mais dans tous les cas le partage de conventions communes et l'adoption large de ces conventions est un élément déterminant pour le succès. En effet, la décentralisation demande une coordination entre plusieurs acteurs contrairement aux architectures centralisée où un acteur a les pleins contrôles lui permettant de simplement orchestrer sa solution. La facilité de l'échange, de la communication et du partage sans silos doit donc être particulièrement efficace.

Dans cette rencontre francophone de l'infonuagique décentralisée, nous proposons d'explorer le potentiel des technologie d'Identité Numérique Souveraine pour faciliter la collaboration entre projets et groupes travaillant sur l'infonuagique décentralisée.

D'une part, l'identité numérique permet de structurer, sécuriser et d'automatiser l'identification des participants et des groupes. Ce qui pourrait permettre un passage à l'échelle des réseau de confiance et de collaboration, non seulement sur les codes source et le développement, mais également en matière de déploiement, de maintenance et de gestion des infrastructures. Ceci pourrait permettre l'utilisation du devops et du IaaS/gitOps communautaire (non contrôlé par une compagnie).

D'autre part, l'utilisation de Identité numérique pour l'authentification permettrait de faciliter l'adoption large des solutions décentralisées et communautaires en fournisant une méthode de connexion commune (universelle?) pour plusieurs groupes indépendants. Le tout en redonnant les données et le contrôle aux utilisateurs sur leurs informations personnelles. -->


### L'infrastructure du Laboratoire d'Identité Numérique du Canada

<p><a href="https://idlab.org/"><img alt="logo idlab" src="/images/idlab.svg" style="height:100px;"/></a></p>

**Par :** [Jean-François Bourque](https://www.linkedin.com/in/jean-fran%C3%A7ois-bourque-55100b15/) ([IDLab](https://www.linkedin.com/company/idlab-org//))  
**Début :** +6 min  
**Fin :** +20 min

Le [Laboratoire](https://idlab.org/) est un organisme indépendant à but non lucratif qui se consacre à faire progresser la confiance numérique en réduisant les obstacles à l’adoption de l’identité numérique. Le Laboratoire préconise la conformité technique et l’interopérabilité des solutions d’identité numérique centrées sur l’utilisateur. Le Laboratoire n’est pas un incubateur. Il ne développe et ne vend pas de solutions d’identité numérique.

Jean-François nous présente les infrastructures de IDLab et ce qu'elles permettent de réaliser à leurs utilisateurs.

### Revue des registres distribués

<p><img alt="network graph icon" src="/images/graph.png" style="height:100px;"/></p>

**Par :** [Jérémy Viau-Trudel](https://www.linkedin.com/in/jvtrudel/) ([Drave Développement](https://drave.quebec/))  
**Début :** +21 min  
**Fin :** +31 min  

Les registres distribués (ou *distributed ledger*, ou encore *blockchain*) sont une des technologies utilisées aujourd'hui dans la conception de solution d'identité souveraine. 

Jérémy présentera quelques solutions proposés dans la littérature.

### Revue des standards du W3C

<p><a href="https://www.w3.org/"><img alt="logo w3c" src="/images/w3c.png" style="height:100px;"/></a></p>

**Par :** [Quentin](https://quentin.dufour.io/) ([Deuxfleurs](https://deuxfleurs.fr/))  
**Début :** +32 min  
**Fin :** +42 min  

Tout comme ActivityPub pour fédérer nos instances, le W3C s'est intéressé à la gestion d'identité sur Internet.
Ses standards, particulièrement le DID (pour Decentralized Identifier), semblent faire consensus dans les implémentations existantes.

Nous plongerons ensemble dans les documents de standardisation du W3C pour voir ce qu'on y trouve.

### Prototyper ensemble

<p>
<a href="https://hyperledger-indy.readthedocs.io/en/latest/"><img alt="logo idlab" src="/images/indy.png" style="height:100px;"/></a>
<a href="https://openid.net/specs/openid-connect-self-issued-v2-1_0.html"><img alt="logo idlab" src="/images/openid.png" style="height:100px;"/></a>
</p>

**Par :** Tout le monde, animé par [Jérémy Viau-Trudel](https://www.linkedin.com/in/jvtrudel/)  et [Quentin](https://quentin.dufour.io/)  
**Début :** +43 min  
**Fin :** +90 min

Nous avons identifié 2 points d'intérêts en amont de cette conférence : [Hyperledger Indy](https://hyperledger-indy.readthedocs.io/en/latest/) et [did-auth-siop](https://github.com/Sphereon-Opensource/did-auth-siop). Ce temps a pour objectif de discuter de ces points d'intérêts et de voir comment nous pourrions mettre en place des prototypes.

Apportez vos PC et préparez vos environnements de développement, ce sera le moment d'entrer en action !
