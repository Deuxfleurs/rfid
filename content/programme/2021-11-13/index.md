---
title: (Archive, Présentation) 13 novembre 2021 - Fondations pour le décentralisé
summary: "Présentations de l'identité souveraine par Drave, du cluster kubernetes autre/entre hébergé par TeDomum et du logiciel de stockage d'objet, alternative à AWS S3, par Deuxfleurs."
---

**Date** - Samedi 13 Novembre 2021  
**Heure** - 10h à Québec, 16h à Paris  
**Durée** - Environ 1h30, possibilité de discuter librement à la fin  
**Lieu** - En visio sur le Jitsi de Fédi Québec : [https://jitsi.fedi.quebec/infonuagique](https://jitsi.fedi.quebec/infonuagique)  

---

Afin d'inaugurer cette première rencontre de l'infonuagique décentralisée, nous avons 3 présentations centrées sur les fondations de l'infonuagique décentralisée, c'est à dire les briques nécessaires pour pouvoir construire des applications décentralisées. Ricky nous parlera de la partie identité, kaiyou de la gestion des ressources de calcul et Quentin du stockage des données.

## Identité numérique souveraine

![logo carte identité](/images/id.svg)

**Par :** [Ricky Ng-Adam](https://www.linkedin.com/in/rngadam/) ([Drave Développement](https://drave.quebec/))  
**Début :** environ t+10 minutes

Au-delà des cryptomonnaies, les blockchains sont des mécanismes mutualisés d'interopérabilité entre  citoyens et organisations civiles, privées, publiques et parapubliques. La prochaine vague technologique pour la blockchain est la création d'une d'identité numérique souveraine contrôlée par l'individu qui la possède. 


Drave Développement participe comme membre à la fondation Trust Over IP (elle-même sous le chapeau de la fondation Linux).  L'organisation met de l'avant un projet pour améliorer les performances des agents d'identité numérique, en particulier dans le contexte d'objets connectés des villes intelligentes, de l'agriculture et dans le domaine de la santé pour sécuriser les interactions machine-à-machine. 


## Auto^wEntre-hébergement

![logo hepto](/images/hepto.png)

**Par :** kaiyou ([Tedomum](https://tedomum.net/) et [ACIDES](https://acides.org/))  
**Début :** environ t+30 minutes

*Host thyself* (Héberge-toi). Un projet de vie et autant de vocations. Pourtant bienfondé, sauf à généraliser le peer-to-peer - tout un programme également -, l'adage ignore celleux en nombre qui n'ont pas les moyens, les compétences, le temps, la motivation. Mutualiser est essentiel : pour distribuer la charge, améliorer la resilience, pour s'entre-héberger plutôt que de s'auto-héberger. Hepto s'attache à rapprocher les deux mondes, en créant des clusters mutualisés faits d'autant de matériels auto-hébergés et en y facilitant la collaboration ; c'est en tout cas notre objectif !

## Mille et une façons d'utiliser Garage

![logo garage](/images/garage.svg)

**Par :** [Quentin](https://quentin.dufour.io/) ([Deuxfleurs](https://deuxfleurs.fr))  
**Début :** environ t+50 minutes

Garage est une solution de stockage d'objet : derrière ce nom compliqué, se trouve un outil qui permet l'hébergement de votre site web, la gestion des données de votre application web préférée ou encore de stocker vos sauvegardes et le tout réparti sur autant de serveurs que nécessaires. La présentation sera axée sur les démonstrations : nous verrons comment déployer un site web statique, commet configurer Nextcloud et Peertube et comment faire ses sauvegardes avec rclone, le tout avec Garage. Le développement de Garage est actuellement financé par le programme [NGI Pointer](https://www.ngi.eu/ngi-projects/ngi-pointer/) de l'Union Européennne.

[Support de présentation](https://rfid.deuxfleurs.fr/presentations/2021-11-13/garage/) - [Enregistrement vidéo](https://video.tedomum.net/w/moYKcv198dyMrT8hCS5jz9)
