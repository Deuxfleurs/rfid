---
title: "Rencontres Francophones de l'Infonuagique Décentralisée"
date: 2018-11-28T15:14:39+10:00
---

Ces rencontres ont pour but de discuter des enjeux techniques de la création d'un Internet plus décentralisé et éthique.

Aucune inscription n'est nécessaire.

## Prochaine rencontre

Samedi 11 décembre 2021 - 10h à Québec - 16h à Paris  
Rendez-vous sur https://jitsi.fedi.quebec/infonuagique

## Organisation

<div style="display: flex; justify-content: space-evenly">

<div style="max-width: 50%">
<img style="height: 100px; margin-bottom: 1rem" src="/images/deuxfleurs.svg" />
<p style="width: 95%">
<a href="https://deuxfleurs.fr"/>Deuxfleurs</a> est un hébergeur associatif français expérimental. Nous cultivons notre autonomie en développant nos propres outils. Nous menons une réflexion écologique et sociale sur nos usages. Nous souhaitons faire du numérique un outil émancipateur.
</p>
</div>

<div style="max-width: 50%">
<img style="height: 100px; margin-bottom: 1rem" src="/images/drave.png" />

<p style="width: 95%">
<a href="https://drave.quebec/">Drave Développement</a> est un organisme à but non lucratif (OBNL) rassemblant une communauté d'individus mobilisée pour répondre aux besoins numériques du Québec, par la promotion des données ouvertes et du logiciel libre afin d'atteindre la souveraineté numérique.
</p>
</div>

</div>

<br>

Pour rejoindre la liste de diffusion, nous poser des questions, proposer un sujet ou juste discuter, écrivez-nous à :  
rfid-organisation (arobase) deuxfleurs.fr

Nous avons également un salon de discussion décentralisé accessible depuis [Matrix](https://matrix.to/#/#rfid:deuxfleurs.fr), le [Rocketchat de Drave](https://rocketchat.drave.quebec/) et le [Discord de La Console](https://discord.com/invite/YJ9q3D24Hf).
